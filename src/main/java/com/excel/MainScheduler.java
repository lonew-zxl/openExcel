package com.excel;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * 创建任务并调度
 * @author 张鑫磊
 * @Date 2020/11/16
 */
public class MainScheduler {


    public static void schedulerJob() throws SchedulerException{
        //创建任务
        JobDetail jobDetail = JobBuilder.newJob(MyJob.class).withIdentity("myJob").build();
        //每秒触发一次任务
        System.out.println("定时表达式："+ConfigUtil.getTime());
        CronTrigger trigger = (CronTrigger) TriggerBuilder.newTrigger()
                .withIdentity("myJob")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(ConfigUtil.getTime())
                )
                .build();
        //创建Schedule实例
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler scheduler = schedulerFactory.getScheduler();
        scheduler.start();

        scheduler.scheduleJob(jobDetail, trigger);
        
    }
    
    public static void main(String[] args) throws SchedulerException {
        MainScheduler mainScheduler = new MainScheduler();
        mainScheduler.schedulerJob();
    }

} 