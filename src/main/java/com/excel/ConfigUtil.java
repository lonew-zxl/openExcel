package com.excel;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
/**
 * 配置文件工具类
 * @author 张鑫磊
 * @Date 2020/11/16
 */
public class ConfigUtil {

    private static Properties prop = new Properties();
    static {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        InputStream is = null;
        is = classLoader.getResourceAsStream("config.properties");
        if (is != null) {
            try {
                prop.load(new InputStreamReader(is, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static String path = (String) prop.get("path");
    private static String time = (String) prop.get("time");


    public static String getPath() {
        return path;
    }

    public static void setPath(String path) {
        ConfigUtil.path = path;
    }

    public static String getTime() {
        return time;
    }

    public static void setTime(String time) {
        ConfigUtil.time = time;
    }

    public static void main(String[] args) {
        System.out.println(ConfigUtil.path);
        System.out.println(ConfigUtil.time);
    }
}
