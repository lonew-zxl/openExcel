package com.excel;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 * 创建要执行的任务
 * @author 张鑫磊
 * @Date 2020/11/16
 */
public class MyJob implements Job{

    public void execute(JobExecutionContext context) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        System.out.println(sdf.format(new Date())+":"+ConfigUtil.getPath());

        ExcelUtil excelUtil = new ExcelUtil();
        //读取excel数据
        ExcelBean excelBean = excelUtil.readExcelToObj(ConfigUtil.getPath());

        System.out.println(JSONObject.toJSONString(excelBean));
    }

}