package com.excel;

import java.util.List;
import java.util.Map;

/**
 * excel的bean
 * @author 张鑫磊
 * @Date 2020/11/16
 */
public class ExcelBean {

    private Map<String,List<FenHangBean>> firstBeanMap;//一级分行
    private Map<String,List<FenHangBean>> secondBeanMap;//二级分行
    private Map<String,List<FenHangBean>> placeBeanMap;//异地分行
    private Map<String,List<FenHangBean>> unknowBeanMap;//没有分行级别表头全部归到此类

    public Map<String, List<FenHangBean>> getUnknowBeanMap() {
        return unknowBeanMap;
    }

    public void setUnknowBeanMap(Map<String, List<FenHangBean>> unknowBeanMap) {
        this.unknowBeanMap = unknowBeanMap;
    }

    public Map<String, List<FenHangBean>> getFirstBeanMap() {
        return firstBeanMap;
    }

    public void setFirstBeanMap(Map<String, List<FenHangBean>> firstBeanMap) {
        this.firstBeanMap = firstBeanMap;
    }

    public Map<String, List<FenHangBean>> getSecondBeanMap() {
        return secondBeanMap;
    }

    public void setSecondBeanMap(Map<String, List<FenHangBean>> secondBeanMap) {
        this.secondBeanMap = secondBeanMap;
    }

    public Map<String, List<FenHangBean>> getPlaceBeanMap() {
        return placeBeanMap;
    }

    public void setPlaceBeanMap(Map<String, List<FenHangBean>> placeBeanMap) {
        this.placeBeanMap = placeBeanMap;
    }
}
