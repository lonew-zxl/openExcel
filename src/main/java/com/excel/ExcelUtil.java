package com.excel;
 
import java.io.File;
import java.io.IOException;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.util.CellRangeAddress;
/**
 * 读取excel文件
 * @author 张鑫磊
 * @Date 2020/11/16
 */
public class ExcelUtil {

    public static void main(String[] args){
        ExcelUtil excelUtil = new ExcelUtil();
        //读取excel数据
        ExcelBean excelBean = excelUtil.readExcelToObj("/Users/lonew/Desktop/123/武汉分行柜台网点分布一览表.xlsx");
        System.out.println(JSONObject.toJSONString(excelBean));
    }

    /**
     * 读取excel数据
     * @param path
     */
    public ExcelBean readExcelToObj(String path) {
        Workbook wb = null;
        List<FenHangBean> list = null;
        ArrayList<Map<String,String>> result = null;
        ExcelBean excelBean = null;
        try {
            wb = WorkbookFactory.create(new File(path));
            excelBean = readExcel(wb, 0, 0, 0);

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return excelBean;
    }
 
    /**
     * 读取excel文件
     * @param wb
     * @param sheetIndex sheet页下标：从0开始
     * @param startReadLine 开始读取的行:从0开始
     * @param tailLine 去除最后读取的行
     */
    private ExcelBean readExcel(Workbook wb,int sheetIndex, int startReadLine, int tailLine) {
        Sheet sheet = wb.getSheetAt(sheetIndex);
        Row row = null;

        ExcelBean excelBean = new ExcelBean();

        String nowBean = "unknow";//初始化时设置未知分行级别
        Map<String,List<FenHangBean>> firstBenaMap = new HashMap<String, List<FenHangBean>>();
        Map<String,List<FenHangBean>> secondBenaMap = new HashMap<String, List<FenHangBean>>();
        Map<String,List<FenHangBean>> placeBenaMap = new HashMap<String, List<FenHangBean>>();
        Map<String,List<FenHangBean>> unknowBeanMap = new HashMap<String, List<FenHangBean>>();
        //初始化未知分行级别列表
        unknowBeanMap.put(nowBean,new ArrayList<FenHangBean>());
        for(int i=startReadLine; i<sheet.getLastRowNum()-tailLine+1; i++) {
            row = sheet.getRow(i);
            FenHangBean fenHangBean = null;
            for(Cell c : row) {
                String returnStr = "";
                boolean isMerge = isMergedRegion(sheet, i, c.getColumnIndex());
                //判断是否具有合并单元格
                if(isMerge) {
                    //获取合并单元格的值
                    String rs = getMergedRegionValue(sheet, row.getRowNum(), c.getColumnIndex());
                    returnStr = rs;
                    if (returnStr.startsWith("一级分行")){
                        //如果合并单元格的值是以一级分行开头，说明出现了一级分行的级别
                        if (!firstBenaMap.containsKey(returnStr)){
                            //如果map中没有这个级别key，说明是首次出现，创建对应分行的列表
                            firstBenaMap.put(returnStr,new ArrayList<FenHangBean>());
                        }
                        nowBean = returnStr;//记录该行以下的key
                        break;//这一行只有分行级别表头，跳出该行的循环
                    }
                    if (returnStr.startsWith("二级分行")){
                        if (!firstBenaMap.containsKey(returnStr)){
                            secondBenaMap.put(returnStr,new ArrayList<FenHangBean>());
                        }
                        nowBean = returnStr;
                        break;
                    }
                    if (returnStr.startsWith("异地支行")){
                        if (!firstBenaMap.containsKey(returnStr)){
                            placeBenaMap.put(returnStr,new ArrayList<FenHangBean>());
                        }
                        nowBean = returnStr;
                        break;
                    }
                }else {
                    c.setCellType(CellType.STRING);
                    returnStr = c.getRichStringCellValue().getString();
                }

                if (c.getColumnIndex()==0){
                    if (returnStr.matches("[0-9]+")){
                        //如果第一列是数字，说明该行是分行内容，读取其他列内容
                        fenHangBean = new FenHangBean();
                        fenHangBean.setIndex(returnStr);
                    }else{
                        //如果不是数字，说明是空行或者表头行等其他可以忽略的东西，跳出该列循环
                        break;
                    }
                }else if(c.getColumnIndex()==1 && fenHangBean != null){
                    fenHangBean.setQuYu(returnStr);
                }else if(c.getColumnIndex()==2 && fenHangBean != null){
                    fenHangBean.setJiGouMingCheng(returnStr);
                }else if(c.getColumnIndex()==3 && fenHangBean != null){
                    fenHangBean.setZhiFuXiTongHangHao(returnStr);
                }else if(c.getColumnIndex()==4 && fenHangBean != null){
                    fenHangBean.setLianXiDianHua(returnStr);
                }else if(c.getColumnIndex()==5 && fenHangBean != null){
                    fenHangBean.setTimeDuiSi(returnStr);
                }else if(c.getColumnIndex()==6 && fenHangBean != null){
                    fenHangBean.setTimeDuiGong(returnStr);
                }else if(c.getColumnIndex()==7 && fenHangBean != null){
                    fenHangBean.setTimeShuangXiu(returnStr);
                }else if(c.getColumnIndex()==8 && fenHangBean != null){
                    fenHangBean.setDiZhi(returnStr);
                }else if(c.getColumnIndex()==9 && fenHangBean != null){
                    fenHangBean.setDiZhiMiaoShu(returnStr);
                }else if (c.getColumnIndex() == 10 && fenHangBean != null){
                    fenHangBean.setGongJiaoLuXian(returnStr);
                }else if(c.getColumnIndex() == 11 && fenHangBean != null){
                    fenHangBean.setJiGouHaoMa(returnStr);
                }else if(c.getColumnIndex() == 12 && fenHangBean != null){
                    fenHangBean.setJiaoHuanHao(returnStr);
                }else if(c.getColumnIndex() == 13 && fenHangBean != null) {
                    fenHangBean.setKaiYeRiQi(returnStr);
                }else if(c.getColumnIndex() == 14 && fenHangBean != null) {
                    fenHangBean.setYouZhengBianMa(returnStr);
                }else if (c.getColumnIndex() == 15 && fenHangBean != null){
                    fenHangBean.setJingDuZuoBiao(returnStr);
                }else if(c.getColumnIndex() == 16 && fenHangBean != null) {
                    fenHangBean.setWeiDuZuoBiao(returnStr);
                }
            }
            if (fenHangBean != null){
                if (nowBean.startsWith("一级分行")){
                    firstBenaMap.get(nowBean).add(fenHangBean);
                }
                if (nowBean.startsWith("二级分行")){
                    secondBenaMap.get(nowBean).add(fenHangBean);
                }
                if (nowBean.startsWith("异地支行")){
                    placeBenaMap.get(nowBean).add(fenHangBean);
                }
                if (nowBean.startsWith("unknow")){
                    unknowBeanMap.get(nowBean).add(fenHangBean);
                }
            }

        }
        excelBean.setFirstBeanMap(firstBenaMap);
        excelBean.setSecondBeanMap(secondBenaMap);
        excelBean.setPlaceBeanMap(placeBenaMap);
        excelBean.setUnknowBeanMap(unknowBeanMap);
        return excelBean;
    }
 
    /**
     * 获取合并单元格的值
     * @param sheet
     * @param row
     * @param column
     * @return
     */
    public String getMergedRegionValue(Sheet sheet ,int row , int column){
        int sheetMergeCount = sheet.getNumMergedRegions();
        for(int i = 0 ; i < sheetMergeCount ; i++){
            CellRangeAddress ca = sheet.getMergedRegion(i);
            int firstColumn = ca.getFirstColumn();
            int lastColumn = ca.getLastColumn();
            int firstRow = ca.getFirstRow();
            int lastRow = ca.getLastRow();
            if(row >= firstRow && row <= lastRow){
                if(column >= firstColumn && column <= lastColumn){
                    Row fRow = sheet.getRow(firstRow);
                    Cell fCell = fRow.getCell(firstColumn);
                    return getCellValue(fCell) ;
                }
            }
        }
        return null ;
    }

    /**
     * 判断指定的单元格是否是合并单元格
     * @param sheet
     * @param row 行下标
     * @param column 列下标
     * @return
     */
    private boolean isMergedRegion(Sheet sheet,int row ,int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if(row >= firstRow && row <= lastRow){
                if(column >= firstColumn && column <= lastColumn){
                    return true;
                }
            }
        }
        return false;
    }
 

 
    /**
     * 获取单元格的值
     * @param cell
     * @return
     */
    public String getCellValue(Cell cell){

        if(cell == null) return "";
        if(cell.getCellType() == Cell.CELL_TYPE_STRING){
            return cell.getStringCellValue();
        }else if(cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){
            return String.valueOf(cell.getBooleanCellValue());
        }else if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){
            return cell.getCellFormula() ;
        }else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
            return String.valueOf(cell.getNumericCellValue());
        }
        return "";
    }
}