package com.excel;
/**
 * 分行对象bean，除序号以外全部拼音命名
 * @author 张鑫磊
 * @Date 2020/11/16
 */
public class FenHangBean {
    private String index;
    private String quYu;
    private String jiGouMingCheng;
    private String zhiFuXiTongHangHao;
    private String lianXiDianHua;
    private String timeDuiSi;
    private String timeDuiGong;
    private String timeShuangXiu;
    private String diZhi;
    private String diZhiMiaoShu;
    private String gongJiaoLuXian;
    private String jiGouHaoMa;
    private String jiaoHuanHao;
    private String kaiYeRiQi;
    private String youZhengBianMa;
    private String jingDuZuoBiao;
    private String weiDuZuoBiao;

    public String getZhiFuXiTongHangHao() {
        return zhiFuXiTongHangHao;
    }

    public void setZhiFuXiTongHangHao(String zhiFuXiTongHangHao) {
        this.zhiFuXiTongHangHao = zhiFuXiTongHangHao;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getQuYu() {
        return quYu;
    }

    public void setQuYu(String quYu) {
        this.quYu = quYu;
    }

    public String getJiGouMingCheng() {
        return jiGouMingCheng;
    }

    public void setJiGouMingCheng(String jiGouMingCheng) {
        this.jiGouMingCheng = jiGouMingCheng;
    }

    public String getLianXiDianHua() {
        return lianXiDianHua;
    }

    public void setLianXiDianHua(String lianXiDianHua) {
        this.lianXiDianHua = lianXiDianHua;
    }

    public String getTimeDuiSi() {
        return timeDuiSi;
    }

    public void setTimeDuiSi(String timeDuiSi) {
        this.timeDuiSi = timeDuiSi;
    }

    public String getTimeDuiGong() {
        return timeDuiGong;
    }

    public void setTimeDuiGong(String timeDuiGong) {
        this.timeDuiGong = timeDuiGong;
    }

    public String getTimeShuangXiu() {
        return timeShuangXiu;
    }

    public void setTimeShuangXiu(String timeShuangXiu) {
        this.timeShuangXiu = timeShuangXiu;
    }

    public String getDiZhi() {
        return diZhi;
    }

    public void setDiZhi(String diZhi) {
        this.diZhi = diZhi;
    }

    public String getDiZhiMiaoShu() {
        return diZhiMiaoShu;
    }

    public void setDiZhiMiaoShu(String diZhiMiaoShu) {
        this.diZhiMiaoShu = diZhiMiaoShu;
    }

    public String getGongJiaoLuXian() {
        return gongJiaoLuXian;
    }

    public void setGongJiaoLuXian(String gongJiaoLuXian) {
        this.gongJiaoLuXian = gongJiaoLuXian;
    }

    public String getJiGouHaoMa() {
        return jiGouHaoMa;
    }

    public void setJiGouHaoMa(String jiGouHaoMa) {
        this.jiGouHaoMa = jiGouHaoMa;
    }

    public String getJiaoHuanHao() {
        return jiaoHuanHao;
    }

    public void setJiaoHuanHao(String jiaoHuanHao) {
        this.jiaoHuanHao = jiaoHuanHao;
    }

    public String getKaiYeRiQi() {
        return kaiYeRiQi;
    }

    public void setKaiYeRiQi(String kaiYeRiQi) {
        this.kaiYeRiQi = kaiYeRiQi;
    }

    public String getYouZhengBianMa() {
        return youZhengBianMa;
    }

    public void setYouZhengBianMa(String youZhengBianMa) {
        this.youZhengBianMa = youZhengBianMa;
    }

    public String getJingDuZuoBiao() {
        return jingDuZuoBiao;
    }

    public void setJingDuZuoBiao(String jingDuZuoBiao) {
        this.jingDuZuoBiao = jingDuZuoBiao;
    }

    public String getWeiDuZuoBiao() {
        return weiDuZuoBiao;
    }

    public void setWeiDuZuoBiao(String weiDuZuoBiao) {
        this.weiDuZuoBiao = weiDuZuoBiao;
    }
}
